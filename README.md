# Searchwing Media

SearchWing (Social) Media, Communication and Information issues. Are there upcoming information and communication events? Is there activity regarding media?

## License
All photos and artwork should have an author and a license. Preferred is Creative Commons CC-BY or CC0.

## Persönlichkeitsrechte
Wenn auf Fotos Menschen erkennbar abgebildet sind, dann braucht man die Einwilligung, dass die Fotos auch veröffentlicht werden dürfen. 

